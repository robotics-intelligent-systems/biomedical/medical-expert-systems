# ClearDental
The main repo for holding the applications that run clear.dental suite. The idea is that all applications will be installed in each node

# Official Website
Please check out [https://clear.dental/](https://clear.dental/)

## Module Reel

[![Module Reel](https://i.ytimg.com/vi/rmYnVxdRzsY/default.jpg)](https://www.youtube.com/watch?v=rmYnVxdRzsY "Module Reel")

## Requirements
 * A UNIX (or Linux) environment 
 * Qt 5.14 or higher 
 * libgit2 and libusb
 * For a list of dev packages needed by (K)Ubuntu, see [a list of packages in the dev docs](https://gitlab.com/cleardental/cleardental-documentation/-/blob/master/devDocs/kubuntuPackages.txt)
 * 1920x1080 monitor (will hopefully be lowered later)

## How to compile
### Using Qt Creator
 * Open cleardental.pro
 * Right click on "cleardental-backend" -> "Build cleardental-backend"
 * Go to the shadow build directory (build-cleardental-XXXX-XXXXX)
 * Go to the cleardental-backend directory
 * Run "sudo make install" (or run "make install" as root)
 * Go up a directory
 * Run "make; sudo make install" to install the modules to $PATH
### Using Command Line
 * Run "qmake -recursive cleardental.pro"
 * Go to the "cleardental-backend"
 * Run "make;sudo make install" (or run the command as root)
 * Go up a directory
 * Run "make; sudo make install" to install the modules to $PATH

## How to Run Clear.Dental
 * You will need the clearDentalData to be located in $HOME/clearDentalData/
 * Run the Manage Preferences app first and add the users + locations

## Do sooner
 * **Low Priority** Export contacts to linphone
 * **Low Priority** Make a mobile version of aquire radiograph to make it work with mobile devices
 * **Low Priority** Fix radiograph billing for multiple PAs and taking a BW for a crown
 * **Low Priority** Create an estimated production for day / week / month
 
## Do later
 * **Low Priority** Easier Pano add and complete?
 * **Low Priority** Start the porting process for Qt6
 * **Low Priority** In adding an appointment in the schedule, be able to add in a limited easily
 * **Low Priority** Remove the concept of a treatment "watch"; it should be an attribute
 * **Low Priority** "Done here" texture in 3D Model
 * **Low Priority** Use tesseract to OCR the card scan to streamline card copy
 * **Low Priority** Generate email for new patient on schedule 
 * **Low Priority** Voice activation for accepting or rejecting a radiograph
 * **Low Priority** Work on CBCT at some point....
 * **Low Priority** Fast redo button for restorations (go based on case note objects)
 * **Low Priority** Add support for Dolphin Emu launcher
 * **Low Priority** Add in beeps when we are overtime
 * **Low Priority** Add in integration to square payments
 * **Low Priority** We need a dentogram panel
 * **Low Priority** In the case notes, procedure objects shouldn't be strings, just keep them as a JSON obj
 * **Low Priority** Create a proper startup script that will work in any dental practice
